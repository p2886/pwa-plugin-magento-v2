# Overview

This module is an add-on for the official Magento progressive web application (PWA) Venia theme. If you are using the Venia theme on your storefront, follow the installation steps below to add Peach Payments as a payment option on checkout.

## Installation guide

Set up a PWA Studio storefront by following the steps in this [installation guide](https://developer.adobe.com/commerce/pwa-studio/tutorials/setup-storefront/).

Go to the `extension` folder and install:

```
yarn add "git+https://gitlab.com/p2886/pwa-plugin-magento-v2.git" -D
```

or update the `package.json`'s `devDependencies`:

```
"@peachpayments/hosted-payment-integration": "git+https://gitlab.com/p2886/pwa-plugin-magento-v2.git",
```

## Server setup guide

Create a PWA project:

```
yarn create @magento/pwa
```

### Create a PWA Studio project

- Project root directory, which is created if it does not exist: `pwa`
- Project short name to use in the `name` field of `package.json`: `pwa`
- Author name to use in the `author` field of `package.json`: `ubuntu`
- Which template would you like to use to bootstrap PWA? Defaults to `"@magento/venia-concept". @magento/venia-concept`
- Magento instance to use as a backend, this is added to the `.env` file: `Other`
- Magento backend URL, this is added to the `.env` file: `https://magento2.localhost`
- Magento store edition (Adobe Commerce or Magento Open Source): `CE`
- Braintree API token to use to communicate with your Braintree instance, this is added to the `.env` file: `sandbox_8yrzsvtm_s2bg8fs563crhqzk`
- NPM package management client to use: `yarn`
- Install package dependencies with Yarn after creating project: `Yes`

1. Add devDependencies to `package.json`:

   ```
   "@peachpayments/hosted-payment-integration": "git+https://gitlab.com/p2886/pwa-plugin-magento-v2.git",
   ```

2. Update `.env` file and set `STAGING_SERVER_PORT`.
3. Update NGINX configuration proxy with the port number:

   ```
   sudo npm install pm2 -g
   ```

4. Launch daemon to run Venia in background:

   ```
   pm2 start "yarn start" --name pwa
   ```
