
import {gql} from '@apollo/client';
import {OrderConfirmationPageFragment} from '@magento/peregrine/lib/talons/CheckoutPage/OrderConfirmationPage/orderConfirmationPageFragments.gql';

export const GET_PEACH_HOSTED_REDIRECT = gql`
    query getPeachHostedRedirectUrl($cartId: String!, $returnUrl: String!){
      getPeachHostedRedirectUrl(input: {
        cart_id: $cartId,
        return_url: $returnUrl
      }) {
        form_data
        form_link
      }
    }
`;

export const GET_PEACH_HOSTED_ORDER_STATUS = gql`
    query getPeachHostedOrderStatus($orderNumber: String!) {
        getPeachHostedOrderStatus(input: { order_number: $orderNumber }) {
            status
        }
    }
`;

export const GET_ORDER_DETAILS = gql`
    query getPeachHostedOrderDetailsData($cartId: String!) {
        getPeachHostedOrderDetailsData(cart_id: $cartId) {
            id
            ...OrderConfirmationPageFragment
        }
    }
    ${OrderConfirmationPageFragment}
`;

export default {
    getPeachHostedRedirectUrl: GET_PEACH_HOSTED_REDIRECT,
    getPeachHostedOrderStatus: GET_PEACH_HOSTED_ORDER_STATUS,
    getPeachHostedOrderDetails: GET_ORDER_DETAILS
}
