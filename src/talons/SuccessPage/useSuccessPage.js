
// eslint-disable-next-line no-unused-vars
import React, {useEffect} from "react";
import mergeOperations from "@magento/peregrine/lib/util/shallowMerge";
import DEFAULT_OPERATIONS from "./successPage.gql";
import { useLazyQuery, useQuery } from '@apollo/client';

export const useSuccessPage = props => {
    const operations = mergeOperations(DEFAULT_OPERATIONS, props.operations);
    const { cartId, orderNumber } = props;
    const {
        getPeachHostedRedirectUrl,
        getPeachHostedOrderStatus,
        getPeachHostedOrderDetails
    } = operations;

    const [
        getPeachHostedOrderData,
        {
            data: peachHostedOrderData,
            error: peachHostedOrderError, // @TODO: implement error on frontend !
            loading: peachHostedOrderLoading
        }
    ] = useLazyQuery(getPeachHostedRedirectUrl);

    const {
        data: orderDetailsData,
        error: orderDetailsError,
        loading: orderDetailsLoading
    } = useQuery(getPeachHostedOrderStatus, {
        fetchPolicy: 'cache-and-network',
        skip: !orderNumber,
        variables: {
            orderNumber
        }
    });

    const [
        getOrderDetails,
        { data: placedOrderDetailsData, loading: peachOrderDetailsLoading }
    ] = useLazyQuery(getPeachHostedOrderDetails, {
        fetchPolicy: 'no-cache'
    });

    useEffect(() => {
        async function peachHostedRedirectUrlData() {
            try {
                const returnCheckoutUrl = window.location.origin + '/checkout/success?on=' + orderNumber + '&ci=' + cartId;
                await getPeachHostedOrderData({
                    fetchPolicy: 'cache-and-network',
                    nextFetchPolicy: 'cache-first',
                    skip: !cartId,
                    variables: {
                        cartId,
                        returnUrl: returnCheckoutUrl
                    }
                });
            } catch (err) {
                console.error(
                    'An error occurred during when placing the order',
                    err
                );
            }
        }

        async function getPlacedOrderDetails () {
            try {
                await getOrderDetails({
                    variables: {
                        cartId
                    }
                });
            } catch (err) {
                console.error(
                    'An error occurred during when placing the order',
                    err
                );
            }
        }

        const peachOrderStatus = (orderDetailsData && orderDetailsData.getPeachHostedOrderStatus.status)
        if (!orderDetailsLoading && peachOrderStatus && peachOrderStatus === 3) {
            peachHostedRedirectUrlData();
        }

        if (!orderDetailsLoading && peachOrderStatus && peachOrderStatus === 1) {
            getPlacedOrderDetails();
        }
    }, [
        orderDetailsLoading,
        orderDetailsError,
        getOrderDetails,
        orderDetailsData,
        cartId,
        orderNumber,
        getPeachHostedOrderData
    ]);

    return {
        formData: (peachHostedOrderData && peachHostedOrderData.getPeachHostedRedirectUrl.form_data) || null,
        formLink: (peachHostedOrderData && peachHostedOrderData.getPeachHostedRedirectUrl.form_link) || null,
        orderStatus: (orderDetailsData && orderDetailsData.getPeachHostedOrderStatus.status) || null,
        orderDetailsLoading: orderDetailsLoading,
        peachHostedOrderLoading: peachHostedOrderLoading,
        placedOrderDetails: (placedOrderDetailsData && { cart: placedOrderDetailsData.getPeachHostedOrderDetailsData })
    };
};
