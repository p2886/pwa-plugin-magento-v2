import {gql} from '@apollo/client';

export const PLACE_ORDER = gql`
    mutation placeOrder($cartId: String!) {
        placeOrder(input: { cart_id: $cartId }) {
            order {
                order_number
                payment_method_code
            }
        }
    }
`;

export default {
    placeOrderMutation: PLACE_ORDER,
};
