/**
 * List of constants that will be used for entire module.
 */

export const PEACH_PAYMENT_METHOD_CODE = 'peachpayments_hosted_default';
