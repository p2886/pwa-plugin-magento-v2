/**
 * Custom intercept file for the extension
 * By default you can only use target of @magento/pwa-buildpack.
 *
 * If do want to extend @magento/peregrine or @magento/venia-ui
 * you should add them to peerDependencies to your package.json
 *
 * If you want to add overwrites for @magento/venia-ui components you can use
 * moduleOverrideWebpackPlugin and componentOverrideMapping
 */

module.exports = targets => {
    const { specialFeatures } = targets.of('@magento/pwa-buildpack');
    specialFeatures.tap(flags => {
        /**
         *  We need to activate esModules, cssModules and GQL Queries to allow build pack to load our extension
         * {@link https://magento.github.io/pwa-studio/pwa-buildpack/reference/configure-webpack/#special-flags}.
         */
        flags[targets.name] = {
            esModules: true,
            cssModules: true,
            graphqlQueries: true
        };
    });

    /**
     * Adding a custom checkout talon.
     */
    targets.of('@magento/peregrine').talons.tap(talons => {
        talons.CheckoutPage.useCheckoutPage.wrapWith(
            '@peachpayments/hosted-payment-integration/src/plugins/wrapUseCheckoutPageTalon.js'
        );
    });

    const gatewaysPath = '@peachpayments/hosted-payment-integration/src/components/BasePayment';
    const {
        checkoutPagePaymentTypes,
        routes
    } = targets.of('@magento/venia-ui');

    checkoutPagePaymentTypes.tap(payments =>
        payments.add({
            paymentCode: 'peachpayments_hosted_default',
            importPath: gatewaysPath
        })
    );

    routes.tap(
        routesArray => {
            routesArray.push({
                name: 'PeachPayments Confirmation Order',
                pattern: '/checkout/success', // @TODO: check if need to pass parameters
                path: '@peachpayments/hosted-payment-integration/src/components/SuccessPage'
            });

            return routesArray;
        }
    );
};
