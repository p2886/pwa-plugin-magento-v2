// eslint-disable-next-line no-unused-vars
import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {useCartContext} from '@magento/peregrine/lib/context/cart';
import PEACH_HOSTED_OPERATIONS from '../talons/CheckoutPage/PlaceOrder.gql.js';
import DEFAULT_OPERATIONS from '@magento/peregrine/lib/talons/CheckoutPage/checkoutPage.gql.js';
import {
    useApolloClient,
    useLazyQuery,
    useMutation
} from '@apollo/client';
import mergeOperations from "@magento/peregrine/lib/util/shallowMerge";
import {useGoogleReCaptcha} from "@magento/peregrine/lib/hooks/useGoogleReCaptcha";
import CheckoutError from "@magento/peregrine/lib/talons/CheckoutPage/CheckoutError";
import { PEACH_PAYMENT_METHOD_CODE } from '../config.js';

const wrapUseCheckoutPageTalon = (original) => {
    return function useCheckoutPage(...args) {
        const operations = mergeOperations(DEFAULT_OPERATIONS, PEACH_HOSTED_OPERATIONS ,args.operations);
        const [{ cartId }, { createCart, removeCart }] = useCartContext();
        const apolloClient = useApolloClient();
        const defaultReturnData = original(...args);

        const {
            createCartMutation,
            getOrderDetailsQuery,
            placeOrderMutation
        } = operations;

        // eslint-disable-next-line no-unused-vars
        const { generateReCaptchaData, recaptchaWidgetProps } = useGoogleReCaptcha({
            currentForm: 'PLACE_ORDER',
            formAction: 'placeOrder'
        });

        const [isPlacingOrder, setIsPlacingOrder] = useState(false);
        // eslint-disable-next-line no-unused-vars
        const [placeOrderButtonClicked, setPlaceOrderButtonClicked] = useState(
            false
        );
        const [fetchCartId] = useMutation(createCartMutation);
        const [
            getOrderDetails,
            { data: orderDetailsData, loading: orderDetailsLoading }
        ] = useLazyQuery(getOrderDetailsQuery, {
            // We use this query to fetch details _just_ before submission, so we
            // want to make sure it is fresh. We also don't want to cache this data
            // because it may contain PII.
            fetchPolicy: 'no-cache'
        });

        const [
            placeOrder,
            {
                data: placeOrderData,
                error: placeOrderError,
                loading: placeOrderLoading
            }
        ] = useMutation(placeOrderMutation);

        const checkoutError = useMemo(() => {
            if (placeOrderError) {
                return new CheckoutError(placeOrderError);
            }
        }, [placeOrderError]);

        const handlePlaceOrder = useCallback(async () => {
            // Fetch order details and then use an effect to actually place the
            // order. If/when Apollo returns promises for invokers from useLazyQuery
            // we can just await this function and then perform the rest of order
            // placement.
            await getOrderDetails({
                variables: {
                    cartId
                }
            });

            setPlaceOrderButtonClicked(true);
            setIsPlacingOrder(true);
        }, [cartId, getOrderDetails]);

        useEffect(() => {
            async function placeOrderAndCleanup() {
                try {
                    const reCaptchaData = await generateReCaptchaData();

                    const result = await placeOrder({
                        variables: {
                            cartId
                        },
                        ...reCaptchaData
                    });

                    if (result) {
                        const orderData = result.data;
                        const orderNumber = orderData.placeOrder.order.order_number;
                        const paymentCode = orderData.placeOrder.order.payment_method_code;

                        // Cleanup stale cart and customer info.
                        await removeCart();
                        await apolloClient.clearCacheData(apolloClient, 'cart');

                        await createCart({
                            fetchCartId
                        });

                        if (paymentCode && paymentCode === PEACH_PAYMENT_METHOD_CODE) {
                            return window.location.href = '/checkout/success?ci='+cartId+'&on='+orderNumber;
                        }
                    }
                } catch (err) {
                    console.error(
                        'An error occurred during when placing the order',
                        err
                    );
                    setPlaceOrderButtonClicked(false);
                }
            }

            if (orderDetailsData && isPlacingOrder) {
                setIsPlacingOrder(false);
                placeOrderAndCleanup();
            }
        }, [
            apolloClient,
            cartId,
            createCart,
            fetchCartId,
            generateReCaptchaData,
            orderDetailsData,
            placeOrder,
            removeCart,
            isPlacingOrder
        ]);

        return {
            ...defaultReturnData,
            error: checkoutError,
            handlePlaceOrder: handlePlaceOrder,
            hasError: !!checkoutError,
            orderDetailsData,
            orderDetailsLoading,
            orderNumber: (placeOrderData && placeOrderData.placeOrder.order.order_number
                && (placeOrderData.placeOrder.order.payment_method_code !== PEACH_PAYMENT_METHOD_CODE))
                ? placeOrderData.placeOrder.order.order_number
                : null, // (placeOrderData && placeOrderData.placeOrder.order.order_number) || null,
            placeOrderLoading
        };
    }
}

export default wrapUseCheckoutPageTalon;
