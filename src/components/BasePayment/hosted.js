
import React from 'react';
import { useStyle } from '@magento/venia-ui/lib/classify';
import { shape, string, bool, func } from 'prop-types';
import BillingAddress from '@magento/venia-ui/lib/components/CheckoutPage/BillingAddress';

import {useHostedPayment} from '../../talons/Payment/useHostedPayment';
import defaultClasses from './hosted.module.css';
import {FormattedMessage} from 'react-intl';

/**
 * The HostedPayment component renders all information to handle histed payment.
 *
 * @param {Boolean} props.shouldSubmit boolean value which represents if a payment nonce request has been submitted
 * @param {Function} props.onPaymentSuccess callback to invoke when the payment nonce has been generated
 * @param {Function} props.onPaymentError callback to invoke when component throws an error
 * @param {Function} props.resetShouldSubmit callback to reset the shouldSubmit flag
 */
const HostedPayment = props => {
    const classes = useStyle(defaultClasses, props.classes);
    const {
        resetShouldSubmit,
        onPaymentSuccess,
        onPaymentError
    } = props;

    const {
        onBillingAddressChangedError,
        onBillingAddressChangedSuccess
    } = useHostedPayment({
        resetShouldSubmit,
        onPaymentSuccess,
        onPaymentError
    });

    return (
        <div className={classes.root}>
            <div className={classes.paymentMethodLogoBackground} />
            <p className={classes.note}>
                <FormattedMessage
                    id={'hostedPayment.note'}
                    defaultMessage={'You will be redirected to our partner\'s website (PeachPayments), where you can safely pay.'}
                />
            </p>
            <br/>
            <BillingAddress
                resetShouldSubmit={props.resetShouldSubmit}
                shouldSubmit={props.shouldSubmit}
                onBillingAddressChangedError={onBillingAddressChangedError}
                onBillingAddressChangedSuccess={onBillingAddressChangedSuccess}
            />
        </div>
    );
};

HostedPayment.propTypes = {
    classes: shape({root: string}),
    shouldSubmit: bool.isRequired,
    onPaymentSuccess: func,
    onPaymentReady: func,
    onPaymentError: func,
    resetShouldSubmit: func.isRequired
};

export default HostedPayment;
