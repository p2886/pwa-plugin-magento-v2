
import React, {useEffect, useRef} from 'react';
import {useSuccessPage} from "../../talons/SuccessPage/useSuccessPage";
import { fullPageLoadingIndicator } from '@magento/venia-ui/lib/components/LoadingIndicator';
import OrderConfirmationPage from '@magento/venia-ui/lib/components/CheckoutPage/OrderConfirmationPage';
import { useLocation } from 'react-router-dom';
import {FormattedMessage} from 'react-intl';
import classes from './successPage.module.css';

// eslint-disable-next-line no-unused-vars
const SuccessPage = props => {
    const form = useRef();
    const { search } = useLocation();
    const orderParams = new URLSearchParams(search);
    const cartId = orderParams.getAll('ci')[0] || null;
    const orderNumber = orderParams.getAll('on')[0] || null;
    const talonProps = useSuccessPage({ cartId, orderNumber });
    const {
        formData,
        formLink,
        orderStatus,
        peachHostedOrderLoading,
        placedOrderDetails
    } = talonProps;

    //@TODO: implement: if status is 3 add delay and resubmit form data again.
    useEffect(()=> {
        if (form && form.current) {
            form.current.submit();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    },[!peachHostedOrderLoading]);

    if ((orderStatus && orderStatus === 1) && (placedOrderDetails)) {
        return (
            <OrderConfirmationPage
                data={ placedOrderDetails }
                orderNumber={ orderNumber }
            />
        );
    }

    if (orderStatus && orderStatus === 2) {
        return (
            <div className={classes.root} data-cy="CartPage-root">
                <h3>
                    <FormattedMessage
                        id={'hostedPayment.declineOrder'}
                        defaultMessage={'Your order has been declined! Please Contact Us for more details.'}
                    />
                </h3>
            </div>
        );
    }

    const redirectFormData = JSON.parse(formData);

    if (formLink && redirectFormData) {
        return (
            <>
                {fullPageLoadingIndicator}
                <form ref={form} action={formLink} method="POST">
                    {redirectFormData ? Object.keys(redirectFormData).map((name, i) =>
                        <input key={i} type="hidden" name={name} value={redirectFormData[name]} />
                    ) : null}
                </form>
            </>
        );
    }

    return fullPageLoadingIndicator;
}

export default SuccessPage;
